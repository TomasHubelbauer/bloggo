# Implement commit hash URL fragment

When rendering a link to a post, the app will append `#c00511` commit hash.
Upon clicking the link, JavaScript will remove the commit hash, because the post
page doesn't do anything with it.
This way posts will be marked as unvisited on each change.

In time maybe consider `?c05511` commit query, which would not get removed but
would actually allow browsing the old version of the page.
